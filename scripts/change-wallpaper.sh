#!/usr/bin/env bash

DISPLAY=:0
WALLPAPER_TO_BE_SET=$(find /home/shadoww/Pictures/wallpapers | grep -E "\.(jpe?g|png)$" | sort -R | sed -e '$!d')
nitrogen $WALLPAPER_TO_BE_SET --set-auto --save --head=0
nitrogen $WALLPAPER_TO_BE_SET --set-auto --save --head=1
